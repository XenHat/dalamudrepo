# Xenhat's Dalamud Repo
[![pipeline status](https://gitlab.com/XenHat/dalamudrepo/badges/main/pipeline.svg)](https://gitlab.com/XenHat/dalamudrepo/-/commits/main)

Merging several repositories into a single one, to reduce network calls overhead.

This is particularly useful for Linux players.

**This repository is only an aggregator and the author takes no responsibility for the use of the provided plugins.**
